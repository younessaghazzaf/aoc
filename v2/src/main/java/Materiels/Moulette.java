package Materiels;

/**
 * Created by controlberkani on 05/02/2016.
 */
public class Moulette implements IMoulette {

    int Value;

    public Moulette(){
        this.Value = 120;
    }
    @Override
    public void setMoulette(int value) {
        this.Value = value;
    }

    @Override
    public int getValue() {
        return Value;
    }
}
