package Materiels;

/**
 * Created by controlberkani on 05/02/2016.
 */
public interface IAfficheur {
    public void setAfficheur(String value);
    public String getValue();

}
