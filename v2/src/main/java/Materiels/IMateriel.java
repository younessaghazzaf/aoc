package Materiels;

/**
 * Created by controlberkani on 05/02/2016.
 */
public interface IMateriel {
    public IAfficheur getAfficheur();
    public Clavier getClavier();
    public IMoulette getMoulette();
}
