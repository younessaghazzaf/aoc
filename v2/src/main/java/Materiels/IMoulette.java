package Materiels;

/**
 * Created by controlberkani on 05/02/2016.
 */
public interface IMoulette {
    public void setMoulette(int value);
    public int getValue();
}
