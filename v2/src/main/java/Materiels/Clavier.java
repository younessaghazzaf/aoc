package Materiels;

/**
 * Created by controlberkani on 21/01/2016.
 */
public interface Clavier {
    public int touchepressee();
    public void reset();
    public void setValue(int value);
}
