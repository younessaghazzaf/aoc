package Materiels;

/**
 * Created by controlberkani on 05/02/2016.
 */
public class Afficheur implements IAfficheur {
    String value;

    public Afficheur(){
        this.value = "";
    }
    @Override
    public void setAfficheur(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
