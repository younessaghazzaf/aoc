package Commandes;

import Moteur.Moteur;

/**
 * Created by controlberkani on 09/10/2015.
 */
public class Tic implements Command {

    Moteur moteur;
    public Tic(Moteur m){
        this.moteur=m;
    }
    @Override
    public void execute() {
        moteur.tic();
    }
}
