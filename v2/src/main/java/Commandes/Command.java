package Commandes;

/**
 * Created by controlberkani on 02/10/2015.
 */
public interface Command {
    void execute();
}
