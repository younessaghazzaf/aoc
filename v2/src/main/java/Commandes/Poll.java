package Commandes;

import Adapter.Adapter;
import javafx.application.Platform;

/**
 * Created by controlberkani on 21/01/2016.
 */
public class Poll implements Command {
    Adapter adapterButton;
    public Poll(Adapter adapterButton){
        this.adapterButton = adapterButton;
    }
    @Override
    public void execute() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                adapterButton.poll();
            }
        });

    }
}
