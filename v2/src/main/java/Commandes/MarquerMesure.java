package Commandes;

import Controlleur.Controleur;

/**
 * Created by controlberkani on 09/10/2015.
 */
public class MarquerMesure implements Command {
    Controleur controleur ;
    @Override
    public void execute() {
        controleur.MarquerMesure();
    }
    public MarquerMesure(Controleur c){
        this.controleur=c;
    }
}
