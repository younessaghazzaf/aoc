package Commandes;

import Controlleur.Controleur;


/**
 * Created by controlberkani on 09/10/2015.
 */
public class MarquerTempo implements Command {
    Controleur controleur;
    @Override
    public void execute() {
        controleur.MarquerTemps();
    }
    public MarquerTempo(Controleur controleur){
        this.controleur=controleur;
    }
}
