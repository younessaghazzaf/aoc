package Adapter;

import Commandes.Poll;
import Controlleur.ControleurImpl;
import Horloge.*;
import IHM.InterfaceImpl;
import Materiels.Clavier;
import Materiels.IMateriel;
import Materiels.Materiel;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class AdapterImpl implements Adapter {
    int mouletteOldValue = 120;
    ControleurImpl controleur;
    Poll poll;
    Horloge horloge = new HorlogeImpl();
    IMateriel materiel;
    public AdapterImpl(IMateriel materiel, ControleurImpl controleur){
        this.materiel = materiel;
        this.controleur = controleur;
        poll = new Poll(this);
        System.out.println("");
        horloge.activerPeriodiquement(poll,337);
    }



    @Override
    public void poll() {
        switch (materiel.getClavier().touchepressee()){
            case 1:
                controleur.start();
                break;
            case 2:
                controleur.stop();
                break;
            case 3:
                controleur.IncrementerMesure();
                break;
            case 4:
                controleur.decrementerMesure();
                break;
            default:
                break;
        }
        materiel.getClavier().reset();
        if(mouletteOldValue != materiel.getMoulette().getValue()){
            mouletteOldValue = materiel.getMoulette().getValue();
            controleur.setTempo(materiel.getMoulette().getValue()+"");
        }
        

    }
}
