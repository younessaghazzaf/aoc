package Adapter;

import Materiels.Clavier;

/**
 * Created by controlberkani on 21/01/2016.
 */
public interface Adapter {
    /**
     * Verifier si un bouton a été cliqué
     */
    public void poll();
}
