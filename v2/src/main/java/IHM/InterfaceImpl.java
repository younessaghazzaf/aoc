package IHM;

import Commandes.Command;
import Controlleur.Controleur;
import Materiels.IMateriel;
import Moteur.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;


/**
 * Created by trisnawiyatni on 20/10/2015.
 */
public class InterfaceImpl implements Interface {
    @FXML
    TextField tempo;
    @FXML
    Text value;
    @FXML
    Slider tempoSlider;
    @FXML
    javafx.scene.shape.Rectangle ledtic;
    @FXML
    javafx.scene.shape.Rectangle ledtoc;
    private Controleur controleur;
    private Moteur moteur;
    Led led;
    //Commandes
    Command start,stop,inc,dec;
    IMateriel materiel;
    //Sonor
    Sonor sonor;
    public void setControleur(Controleur c, IMateriel materiel){
        this.controleur=c;
        this.materiel = materiel;
    }

    @Override
    public void setMoteur(Moteur m){
        this.moteur=m;
    }

    @Override
    public void init(){
        sonor = new Sonor();
        tempoSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val) {
                System.out.println(new_val.doubleValue());
                materiel.getMoulette().setMoulette(new_val.intValue());
                //controleur.setTempo(new_val.intValue()+"");

            }
        });
    }

    @Override
    public void start(){
        materiel.getClavier().setValue(1);
        //start.execute();
    }

    @Override
    public void stop(){
        materiel.getClavier().setValue(2);
        //stop.execute();
    }

    @Override
    public void increment(){
        materiel.getClavier().setValue(3);
        //inc.execute();
    }

    @Override
    public void decrement(){
        materiel.getClavier().setValue(4);
        //dec.execute();
    }

    @Override
    public void setTempoText(int v){
        this.value.setText(v+"");
    }

    @Override
    public void allumeTic(){

        led=new SimpleLed(ledtic, Color.STEELBLUE);
        led.flash();
        System.out.println("Tic");
    }

    @Override
    public void allumeToc(){
        led=new SimpleLed(ledtoc, Color.ORANGERED);
        led.flash();
        System.out.println("Toc");
    }

    @Override
    public void updateTempo() {
        tempo.setText(moteur.getTempo()+"");
    }

    @Override
    public void setStartCommande(Command commande) {
        start = commande;
    }

    @Override
    public void setStopCommande(Command commande) {
        stop = commande;
    }

    @Override
    public void setIncCommande(Command commande) {
        inc = commande;
    }

    @Override
    public void setDecCommande(Command commande) {
        dec = commande;
    }

}
