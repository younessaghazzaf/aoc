package Controlleur;

import java.util.Observer;


public interface Controleur extends Observer{
    /**
     * Marquer la mésure sur la vue
     */
    void MarquerMesure();

    /**
     * Marquer le temps sur la vue
     */
    void MarquerTemps();

    /**
     * Incrémenter la mésure
     */
    void IncrementerMesure();

    /**
     * Décrementer la mésure
     */
    void decrementerMesure();

    /**
     * Mise à jour du tempo sur la vue
     */
    void updateTempo();

    /**
     * Activer le metronome
     */
    void start();

    /**
     * Désactiver le métronome
     */
    void stop();

    /**
     *
     * @param t  battement par minute
     */
    void setTempo(String t);
}
