package Horloge;

import Commandes.Command;

import java.util.Timer;


public class HorlogeImpl implements Horloge {
    Timer timer;

    @Override
    public void activerApresDelai(Command cmd, long delaiEnSecondes) {

    }

    @Override
    public void activerPeriodiquement(Command cmd,long periodEnSecondes) {
        timer=new Timer();
        TachePeriodique tache=new TachePeriodique(cmd);
        //long tempsMillisecondes = this.tempsEnMilliSecondes(periodEnSecondes);
        //System.out.println(tempsMillisecondes);
        timer.schedule(tache, 0, periodEnSecondes);
    }

    @Override
    public void desactiver() {
        timer.cancel();
        timer.purge();
    }
    /**
     * Renvoi le temps "tempsEnSeconde" en milli secondes
     * @param tempsEnSeconde
     * @return
     */
    private long tempsEnMilliSecondes(float tempsEnSeconde) {
        return (long) (1000 * tempsEnSeconde);
    }
}
