package Horloge;

import Commandes.Command;

/**
 * Created by controlberkani on 09/10/2015.
 */

/*
* Invoker
* */
public interface Horloge {
    void activerApresDelai(Command cmd, long delaiEnSecondes);
    void activerPeriodiquement(Command cmd, long periodEnSecondes);
    void desactiver();
}
