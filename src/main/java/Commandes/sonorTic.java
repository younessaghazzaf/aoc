package Commandes;

import IHM.Sonor;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class sonorTic implements Command{
    Sonor sonor;

    public sonorTic(Sonor sonor){
        this.sonor = sonor;
    }
    @Override
    public void execute() {
        sonor.playSound("/tick.wav");
    }
}
