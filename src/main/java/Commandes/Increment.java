package Commandes;

import Controlleur.Controleur;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class Increment implements Command {
    Controleur controleur;
    public Increment(Controleur controleur){
        this.controleur = controleur;
    }
    @Override
    public void execute() {
        controleur.IncrementerMesure();
    }
}
