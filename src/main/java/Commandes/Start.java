package Commandes;

import Controlleur.Controleur;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class Start implements Command {
    Controleur controleur;
    public Start(Controleur controleur){
        this.controleur = controleur;
    }
    @Override
    public void execute() {
        controleur.start();
    }
}
