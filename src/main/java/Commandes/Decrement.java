package Commandes;

import Controlleur.Controleur;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class Decrement implements Command {
    Controleur controleur;
    public Decrement(Controleur controleur){
        this.controleur = controleur;
    }
    @Override
    public void execute() {
        controleur.decrementerMesure();
    }
}
