package Commandes;

import Controlleur.Controleur;

/**
 * Created by youness on 20/10/15.
 */
public class updateTempo implements Command {
    Controleur controleur;
    @Override
    public void execute() {
        controleur.updateTempo();
    }
    public updateTempo(Controleur c){
        this.controleur=c;
    }
}
