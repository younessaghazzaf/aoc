package Commandes;

import Controlleur.Controleur;

/**
 * Created by controlberkani on 22/01/2016.
 */
public class Stop implements Command {
    Controleur controleur;
    public Stop(Controleur controleur){
        this.controleur = controleur;
    }
    @Override
    public void execute() {
        controleur.stop();
    }
}
