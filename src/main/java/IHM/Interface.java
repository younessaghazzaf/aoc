package IHM;

import Commandes.Command;
import Controlleur.Controleur;
import Moteur.Moteur;


/**
 * Created by controlberkani on 22/01/2016.
 */
public interface Interface {
    /**
     *
     * @param c Controlleur
     */
    public void setControleur(Controleur c);

    /**
     * Moteur
     * @param m
     */
    public void setMoteur(Moteur m);

    /**
     *  Initialiser la configuration de la vue
     */
    public void init();

    /**
     * Acitver le moteur
     */
    public void start();

    /**
     * Arrêter le moteur
     */
    public void stop();

    /**
     * Incrémenter la mésure
     */
    public void increment();

    /**
     * Décrementer la mésure
     */
    public void decrement();

    /**
     *
     * @param v Battement par mésure
     */
    public void setTempoText(int v);

    /**
     * Allumer led tempo
     */
    public void allumeTic();

    /**
     * Allumer led messure
     */
    public void allumeToc();

    /**
     * Mise à jour le tempo sur la vue
     */
    public void updateTempo();

    /**
     *
     * @param commande Commande start
     */
    public void setStartCommande(Command commande);

    /**
     *
     * @param commande Commande stop
     */
    public void setStopCommande(Command commande);

    /**
     *
     * @param commande Commande incremente
     */
    public void setIncCommande(Command commande);

    /**
     *
     * @param commande Commande decremente
     */
    public void setDecCommande(Command commande);

}