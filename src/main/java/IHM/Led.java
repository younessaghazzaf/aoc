package IHM;

/**
 * Created by controlberkani on 26/12/2015.
 */
public interface Led {
    /**
     * rend la led allumé pendant une courte période
     */
    public void flash();
}
