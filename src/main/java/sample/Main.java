package sample;


import Commandes.Decrement;
import Commandes.Increment;
import Commandes.Start;
import Commandes.Stop;
import Controlleur.ControleurImpl;
import IHM.InterfaceImpl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader=new FXMLLoader();
        Parent root = loader.load(getClass().getResource("/metronome.fxml").openStream());
        InterfaceImpl view = loader.getController();

        ControleurImpl controleur = new ControleurImpl();
        controleur.setView(view);
        view.setControleur(controleur);
        view.init();

        view.setStartCommande(new Start(controleur));
        view.setStopCommande(new Stop(controleur));
        view.setIncCommande(new Increment(controleur));
        view.setDecCommande(new Decrement(controleur));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 536, 333));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
        //new ihmImpl(new ControleurImpl());

    }
}
