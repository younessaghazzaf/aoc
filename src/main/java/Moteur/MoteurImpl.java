package Moteur;


import Commandes.*;
import Controlleur.Controleur;
import Horloge.*;

import java.util.Observable;


public class MoteurImpl extends Observable implements Moteur
{
    Controleur controleur;
    Horloge horloge;
    int Tempo=120;
    int index_messure=0;
    Boolean enMarche=false;
    int Messure=4;

    public MoteurImpl(Controleur c){
        this.controleur=c;
        horloge=new HorlogeImpl();
        index_messure=Messure;
    }

    @Override
    public int getTempo() {
        return Tempo;
    }

    @Override
    public void setTempo(int t) {
        Command tic = new Tic(this);
        Command UpdateTempo =new updateTempo(controleur);
        this.Tempo=t;
        if(getEnMarche()){
        horloge.desactiver();
        horloge.activerPeriodiquement(tic,getPeriodMSFromBPM(getTempo()));}
        //horloge.activerApresDelai(tic,getTempo());
        UpdateTempo.execute();
    }


    @Override
    public int getTempsPm() {
        return Messure;
    }

    @Override
    public void setNbTempsPm(int t) {
        Messure = t;
    }

    @Override
    public Boolean getEnMarche() {
        return enMarche;
    }

    @Override
    public void setEnMarche(Boolean m) {
        System.out.println("Engine on : "+ m);
        if(m){
            Command tic=new Tic(this);
            this.horloge.activerPeriodiquement(tic,getPeriodMSFromBPM(getTempo()));
            enMarche=true;
        }else{
            //this.setTempo(Init);
            this.horloge.desactiver();
            enMarche=false;
        }
    }

    @Override
    public void tic() {
        index_messure--;
        if(index_messure==0){
            index_messure=Messure;
            Command command= new MarquerMesure(controleur);
            command.execute();
        }else{
            Command command=new MarquerTempo(controleur);
            command.execute();
        }
    }

    /**
     *
     * @param bpm Battement par minute
     * @return Battement par milliseconde
     */
    private Long getPeriodMSFromBPM(int bpm) {
        float periodMinute = 1.0f / Float.valueOf(bpm);
        Long periodMS = Long.valueOf((int) (periodMinute * 60 * 1000));
        return periodMS;
    }

}

