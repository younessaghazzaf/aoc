package Horloge;

import Commandes.Command;

import java.util.TimerTask;

/**
 * Created by youness on 20/10/15.
 */
public class TachePeriodique extends TimerTask{

    private Command commande;
    @Override
    public void run() {
        commande.execute();
    }
    public TachePeriodique(Command c){
        super();
        commande=c;
    }
}
